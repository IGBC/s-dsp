use num_traits::NumOps;
use num_traits::Zero;

pub trait Numeric: NumOps + Zero + Copy {}

impl Numeric for isize {}
impl Numeric for usize {}

impl Numeric for i8 {}
impl Numeric for u8 {}
impl Numeric for i16 {}
impl Numeric for u16 {}
impl Numeric for i32 {}
impl Numeric for u32 {}
impl Numeric for i64 {}
impl Numeric for u64 {}

impl Numeric for f32 {}
impl Numeric for f64 {}
