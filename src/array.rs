use crate::types::{Numeric};

use core::ops::{Index,IndexMut,Add,Sub,Mul};
use core::slice::SliceIndex;

pub trait IndexType<T>: Sized + SliceIndex<[T], Output = T> {}

#[derive(Debug,Copy,Clone,PartialEq, Eq)]
pub struct Array <T, const N: usize>
    where T: Numeric
    { 
        data: [T; N],
    }

impl<T, const N: usize> Array<T, N> where T:Numeric {
    pub fn new() -> Self {
        Self {
            data: [T::zero();N]
        }
    }

    pub fn new_from(data: &[T; N]) -> Self {
        let data = data.clone();
        Self { data }
    }
} 

impl<T, const N: usize> Index<usize> for Array<T, N> where T: Numeric {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        self.data.index(index)
    }
}

impl<T, const N: usize> IndexMut<usize> for Array<T, N> where T: Numeric {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.data.index_mut(index)
    }
}

impl<T, const N: usize> Add for Array<T, N> where T: Numeric {
    type Output = Array<T, N>;

    fn add(self, rhs: Self) -> Self::Output {
        let mut lv = Self::new();
        
        for i in 0..N {
            lv[i] = self[i] + rhs[i];
        }

        return lv;
    }
}

impl<T, const N: usize> Sub for Array<T, N> where T: Numeric {
    type Output = Array<T, N>;

    fn sub(self, rhs: Self) -> Self::Output {
        let mut lv = Self::new();
        
        for i in 0..N {
            lv[i] = self[i] - rhs[i];
        }

        return lv;
    }
}

impl<T, const N: usize> Mul for Array<T, N> where T: Numeric {
    type Output = Array<T, N>;

    fn mul(self, rhs: Self) -> Self::Output {
        let mut lv = Self::new();
        
        for i in 0..N {
            lv[i] = self[i] * rhs[i];
        }

        return lv;
    }
}

impl<T> IndexType<T> for usize {}

#[cfg(test)]
mod tests {
    use super::Array;
    #[test]
    fn construction() {
        let a: Array<u32, 5> = Array::new();
        for i in 0..5 {
            assert_eq!(a.data[i], 0);
        }
    }

    #[test]
    fn index() {
        let a: Array<usize, 5> = Array::new_from(&[0,1,2,3,4]);
        for i in 0..5 {
            assert_eq!(i, a[i]);
        }
    }

    #[test]
    fn index_mut() {
        let a = Array::new_from(&[0,1,2,3,4]);
        let mut b = Array::new();
        for i in 0..5 {
            b[i] = i;
        }
        assert_eq!(a,b);
    }

    #[test]
    fn add() {
        let a = Array::new_from(&[0,1,2,3,4]);
        let b = Array::new_from(&[5,6,7,8,9]);
        let c = a + b;
        assert_eq!(c[0], 5);
        assert_eq!(c[1], 7);
        assert_eq!(c[2], 9);
        assert_eq!(c[3], 11);
        assert_eq!(c[4], 13);
    }

    #[test]
    fn sub() {
        let a = Array::new_from(&[5,6,7,8,9]);
        let b = Array::new_from(&[0,1,2,3,4]);
        let c = a - b;
        assert_eq!(c[0], 5);
        assert_eq!(c[1], 5);
        assert_eq!(c[2], 5);
        assert_eq!(c[3], 5);
        assert_eq!(c[4], 5);
    }

    #[test]
    fn mul() {
        let a = Array::new_from(&[2.0, 1.0, 3.0]);
        let b = Array::new_from( &[0.5, 0.25, 1.5]);
        let c = a * b;
        assert_eq!(c[0], 1.0);
        assert_eq!(c[1], 0.25);
        assert_eq!(c[2], 4.5);
    }
}