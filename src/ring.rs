use core::ops::{Index, IndexMut, Add, Rem};

use crate::array::Array;
use crate::types::Numeric;

use num_traits::{Bounded, Zero};

pub trait IndexType: Copy + TryInto<usize> + TryFrom<usize> + Add<Output = Self> + Bounded + PartialOrd + Zero + Rem<Output = Self> {}

pub struct Ring<T, const N: usize> where T: Numeric {
    front: usize,
    inner: Array<T, N>
}

impl<T, const N: usize> Ring<T, N> where T: Numeric  {
    pub fn new() -> Self {
        Self {
            front: 0,
            inner: Array::new()
        }
    }

    pub fn new_from(data: &[T; N]) -> Self {
        Self {
            front: 0,
            inner: Array::new_from(data)
        }
    }

    pub fn rotate_left(&mut self, next: T) -> T {
        let pop = self.inner[self.front];
        self.inner[self.front] = next;
        self.front = (self.front + 1) % N;
        pop
    }

    pub fn rotate_right(&mut self, next: T) -> T {
        if self.front == 0 {
            self.front = N -1;
        } else {
            self.front -= 1;
        }
        let pop = self.inner[self.front];
        self.inner[self.front] = next;
        pop
    }

    fn internal_index<I>(&self, index: I) -> usize where I: IndexType, usize: TryFrom<I> {
        unsafe {
            assert!(I::min_value()<= I::zero());
            let n = I::try_from(N).unwrap_unchecked();
            let r = index + self.front.try_into().unwrap_unchecked();
            let mut i = r % n;
            if i < I::zero() {
                i = i + n;
            }
            assert!(i >= I::zero());
            TryInto::<usize>::try_into(i).unwrap_unchecked() % N
        }
    }
}

impl<T, I, const N: usize> Index<I> for Ring<T, N> where T: Numeric, I: IndexType, usize: TryFrom<I> {
    type Output = T;
    fn index(&self, index: I) -> &Self::Output {
        self.inner.index(self.internal_index(index))
    }
}

impl<T, I, const N: usize> IndexMut<I> for Ring<T, N> where T: Numeric, I: IndexType, usize: TryFrom<I> {
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.inner.index_mut(self.internal_index(index))
    }
}

impl IndexType for usize {}
impl IndexType for i32 {}

#[cfg(test)]
mod test {
    use super::Ring;
    #[test]
    fn index_positive() {
        
        let r = Ring::new_from(&[1,2,3,4,5]);
        assert_eq!(r[0], 1);
        assert_eq!(r[1], 2);
        assert_eq!(r[2], 3);
        assert_eq!(r[3], 4);
        assert_eq!(r[4], 5);
        assert_eq!(r[5], 1);
        assert_eq!(r[6], 2);
        assert_eq!(r[7], 3);
        assert_eq!(r[8], 4);
        assert_eq!(r[9], 5);
        assert_eq!(r[10], 1);
    }

    #[test]
    fn index_negitive() {
        
        let r = Ring::new_from(&[1,2,3,4,5]);
        assert_eq!(r[0], 1);
        assert_eq!(r[-1], 5);
        assert_eq!(r[-2], 4);
        assert_eq!(r[-3], 3);
        assert_eq!(r[-4], 2);
        assert_eq!(r[-5], 1);
        assert_eq!(r[-6], 5);
        assert_eq!(r[-7], 4);
        assert_eq!(r[-8], 3);
        assert_eq!(r[-9], 2);
        assert_eq!(r[-10], 1);
    }

    #[test]
    fn rotate_left() {
        let mut r = Ring::new_from(&[1,2,3,4,5]);
        let p = r.rotate_left(6);
        assert_eq!(r[0], 2);
        assert_eq!(r[1], 3);
        assert_eq!(r[2], 4);
        assert_eq!(r[3], 5);
        assert_eq!(r[4], 6);
        assert_eq!(r[-1], 6);
        assert_eq!(r[-2], 5);
        assert_eq!(r[-3], 4);
        assert_eq!(r[-4], 3);
        assert_eq!(r[-5], 2);
        assert_eq!(p,1);
    }

    #[test]
    fn rotate_right() {
        let mut r = Ring::new_from(&[1,2,3,4,5]);
        let p = r.rotate_right(0);
        assert_eq!(r[0], 0);
        assert_eq!(r[1], 1);
        assert_eq!(r[2], 2);
        assert_eq!(r[3], 3);
        assert_eq!(r[4], 4);
        assert_eq!(r[-1], 4);
        assert_eq!(r[-2], 3);
        assert_eq!(r[-3], 2);
        assert_eq!(r[-4], 1);
        assert_eq!(r[-5], 0);
        assert_eq!(p, 5);
    }
}